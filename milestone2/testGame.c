/*
 *  testGame.c
 * Daniel Playfair Cal
 * 15/05/2012
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>


#include "Game.h"

#define DEFAULT_DICE {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2}

// The following values ensure each UNI has an inital campus on MJ and MMON
#define DEFAULT_DISCIPLINES {3,1,3,2,2,2,2,3,3,3,3,5,4,4,4,4,5,5,5}

#define NUM_STUDENT_TYPES  6

#define INITIAL_PATH_A1 ""
#define INITIAL_PATH_A2 "RLLRRLRLRLR"
#define INITIAL_PATH_B1 "RLRRLRR"
#define INITIAL_PATH_B2 "RLRLLRLRLR"
#define INITIAL_PATH_C1 "LRLRL"
#define INITIAL_PATH_C2 "RLRLRLRRLR"


const path initialPaths[6] = {
    INITIAL_PATH_A1,
    INITIAL_PATH_A2,
    INITIAL_PATH_B1,
    INITIAL_PATH_B2,
    INITIAL_PATH_C1,
    INITIAL_PATH_C2
};

typedef struct _students {
    int values[NUM_UNIS+1][NUM_STUDENT_TYPES];
} Students;


void testGO8(void);

void zeaterTest(void);

Students getStudentCounts(Game game);

int main (int argc, const char * argv[]) {
    testGO8();

    zeaterTest();

    printf("All tests passed. You are awesome!\n");
    return EXIT_SUCCESS;
}

// Given a uni, htis function returns a string giving its name
static char* uniString(int uni) {
    char* result;
    if (uni == NO_ONE) {
        result = "NO_ONE";
    } else if (uni == UNI_A) {
        result = "UNI_A";
    } else if (uni == UNI_B) {
        result = "UNI_B";
    } else if (uni == UNI_C) {
        result = "UNI_C";
    } else {
        result = "INVALID_UNI";
    }
    return result;
}

// Given a uni, htis function returns a string giving its name
static char* disciplineString(int discipline) {
    char* result;
    if (discipline == STUDENT_THD) {
        result = "THD";
    } else if (discipline == STUDENT_BPS) {
        result = "BPS";
    } else if (discipline == STUDENT_BQN) {
        result = "BQN";
    } else if (discipline == STUDENT_MJ) {
        result = "MJ";
    } else if (discipline == STUDENT_MTV) {
        result = "MTV";
    } else if (discipline == STUDENT_MMONEY) {
        result = "MON";
    } else {
        result = "Invalid discipline";
    }
    return result;
}

void testGO8(void) {
    printf("testGO8 called.\n");
    
    printf("Creating a new game with all diceValues 2 and with each uni");
    printf("having a MJ and a MMON area next to their initial campuses\n");
    int disciplines[NUM_REGIONS] = DEFAULT_DISCIPLINES;
    int dice[NUM_REGIONS] = DEFAULT_DICE;
    Game game = newGame(disciplines, dice);
    printf("newGame ran successfully\n");

    Students students = getStudentCounts(game);

    printf("\nTesting that it is illegal for anyone to start a spinoff, ");
    printf("build a campus, or build a GO8 with no students...\n");
    for (int i = 0; i <= NUM_UNIS; i++) {
        throwDice(game, 3);
        int uni = getWhoseTurn(game);
        printf("\nFor %s:\n", uniString(uni));

        printf("Testing that it is illegal to build a GO8 at %s\n", initialPaths[2*uni-2]);
        action buildGO8;
        buildGO8.actionCode = BUILD_GO8;
        strcpy(buildGO8.destination, initialPaths[uni]);
        assert(isLegalAction(game, buildGO8) == FALSE);

        printf("Testing that it is illegal to build a GO8 at %s\n", initialPaths[2*uni-1]);
        strcpy(buildGO8.destination, initialPaths[uni]);
        assert(isLegalAction(game, buildGO8) == FALSE);

        printf("Testing that it is illegal to start a spinoff...\n");
        action startSpinoff;
        startSpinoff.actionCode = START_SPINOFF;
        assert(isLegalAction(game, startSpinoff) == FALSE);
    }
    printf("passed.\n");

    printf("\nRolling a 2 3 times...\n");
    for (int i = 0; i < 3; i++) {
        throwDice(game, 2);
    }
    printf("Rolling dice succeeded.\n");

    students = getStudentCounts(game);
    Students correctStudents = {{
        {-1,-1,-1,-1,-1,-1},
        {0,0,0,3,0,3},
        {0,0,0,3,0,3},
        {0,0,0,3,0,3}
    }};
    printf("\nTesting that the student counts are correct...\n");
    for (int uni = UNI_A; uni <= UNI_C; uni++) {
        for (int discipline = STUDENT_THD; discipline <= STUDENT_MMONEY; discipline++) {
            printf("Testing that %s has %d", uniString(uni), correctStudents.values[uni][discipline]);
            printf(" %ss...\n", disciplineString(discipline));
            assert(students.values[uni][discipline] == correctStudents.values[uni][discipline]);
        }
    }

    printf("\nTesting that it is legal for all unis to build a GO8 at ");
    printf("each of their initial campuses...\n");
    for (int i = 0; i < 3; i++) {
        throwDice(game, 3);
        int uni = getWhoseTurn(game);
        action buildGO8;
        buildGO8.actionCode = BUILD_GO8;

        printf("Testing that it is legal for %s to build", uniString(uni));
        printf(" a GO8 at path %s...\n", initialPaths[2*uni-2]);
        strcpy(buildGO8.destination, initialPaths[2*uni-2]);
        assert(isLegalAction(game, buildGO8));

        printf("Testing that it is legal for %s to build", uniString(uni));
        printf(" a GO8 at path %s...\n", initialPaths[2*uni-1]);
        strcpy(buildGO8.destination, initialPaths[2*uni-1]);
        assert(isLegalAction(game, buildGO8));
    }
    printf("passed.\n");

    // these fail after a "null vertex"
    /*assert (getCampus(game, "") == 1);
    assert (getCampus(game, "LB") == 1);
    assert (getCampus(g, "LRRRRR") == 1);*/
    
}

Students getStudentCounts(Game game) {
    printf("\nGetting students counts...\n");
    printf("  UNI    THD    BPS    BQN     MJ    MTV   MMON\n");
    Students students;
    for (int uni = UNI_A; uni <= UNI_C; uni++) {
        printf("%s", uniString(uni));
        for (int discipline = 0; discipline < NUM_STUDENT_TYPES; discipline++) {
            students.values[uni][discipline] = getStudents(game, uni, discipline);
            printf("     %2d", students.values[uni][discipline]);
        }
        printf("\n");
    }
    printf("\n");
    return students;
}



void zeaterTest(void) {
    int disciplines[NUM_REGIONS] = {STUDENT_BQN, STUDENT_MMONEY,
                                    STUDENT_MJ, STUDENT_MMONEY,
                                    STUDENT_MJ, STUDENT_BPS,
                                    STUDENT_MTV, STUDENT_MTV,
                                    STUDENT_BPS, STUDENT_MTV,
                                    STUDENT_BQN, STUDENT_MJ,
                                    STUDENT_BQN, STUDENT_THD,
                                    STUDENT_MJ, STUDENT_MMONEY,
                                    STUDENT_MTV, STUDENT_BQN,
                                    STUDENT_BPS};

    int diceValues[NUM_REGIONS] = {9, 10, 8, 12, 6, 5, 3, 11, 3,
                                   11, 4, 6, 4, 9, 9, 2, 8, 10, 5};

    printf("creating a new Game:\n");
    for (int region = 0; region < NUM_REGIONS; region++) {
        printf("Region %2d: ", region);
        printf("diceValue: %2d ", diceValues[region]);
        printf("discipline: %s\n", disciplineString(disciplines[region]));
    }
                                   
    Game game = newGame(disciplines, diceValues);

    printf("rolling 9, 5\n");
    throwDice(game, 9);
    throwDice(game, 5);
    printf("obtaining arc for UNI_B at \"LRLRRLRLL\"\n");
    action makeArc = {OBTAIN_ARC, "LRLRRLRLL", 0, 0};
    makeAction(game, makeArc);
    printf("rolling 9, 5, 9, 5, 9, 5\n");
    throwDice(game, 9);
    throwDice(game, 5);

    throwDice(game, 9);
    throwDice(game, 5);
    throwDice(game, 9);
    throwDice(game, 5);
    printf("retraining BQNs to an MJ for UNI_B\n");
    action retrainBQNtoMJ = {RETRAIN_STUDENTS, "", STUDENT_BQN, STUDENT_MJ};
    makeAction(game, retrainBQNtoMJ);
    printf("retraining BPSs to an MTV for UNI_B\n");
    action retrainBBStoMTV = {RETRAIN_STUDENTS, "", STUDENT_BPS, STUDENT_MTV};
    makeAction(game, retrainBBStoMTV);

    printf("rolling 9,5,9\n");
    throwDice(game, 9);
    throwDice(game, 5);
    throwDice(game, 9);

    printf("building a campus for UNI_B at \"LRLRRLRL\"\n");
    action buildCampus = {BUILD_CAMPUS, "LRLRRLRL", 0, 0};
    makeAction(game, buildCampus);
    printf("rolling 9\n");
    throwDice(game, 9);
    printf("checking that UNI_B has 2 BQNs\n");
    assert(getStudents(game, UNI_B, STUDENT_BQN) == 2);
}
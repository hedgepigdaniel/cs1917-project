/*
 * Game.c
 * Daniel Playfair Cal, Radin Ahmed
 * 13/05/2012
 * cs1917 - Monday-12-Moog - Adrian Ratter
 * Defines interface functions for the cs1917 project game
 * 
 */

#include "Game.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

// Different types of objects on the board
#define OBJECT_NONE    0
#define OBJECT_VERTEX  1
#define OBJECT_EDGE    2
#define OBJECT_REGION  3

//define VACANT_VERTEX 0
#define NORMAL_CAMPUS  1
#define GO8_CAMPUS     2

#define NUM_REGIONS        19
#define NUM_STUDENT_TYPES   6
#define NUM_VERTICES       54
#define NUM_EDGES          72
#define PATH_LIMIT        150

// When the game hasn't started yet, it is this strange person's turn
#define TERRA_NULLIUS -1

#define TRUE   1
#define FALSE  0

// The number of rows and columns in the board struct. Not alalogous to
// the number of regions - it just has to be big enough to store all the
// objects in the correct layout
#define BOARD_COLUMNS  23
#define BOARD_ROWS     21

// Directions - used for the steps in a path and to describe the vectors
// between neighbouring objects in the board struct. Numbered from the
// right and then anticlockwise according to mathematical convention. Not
// all of these directions are valid in all cases.
#define DIR_RIGHT         0
#define DIR_RIGHT_ABOVE   1
#define DIR_ABOVE         2
#define DIR_LEFT_ABOVE    3
#define DIR_LEFT          4
#define DIR_LEFT_BELOW    5
#define DIR_BELOW         6
#define DIR_RIGHT_BELOW   7

#define NUM_DIRECTIONS    8

// Used when alternative directions (in degrees) are used
#define DEGREES_IN_CIRCLE 360

// The starting grid coordinates and direction (in degrees) for a path
#define PATH_START_LOCATION       {10,20}
#define PATH_INITIAL_DIRECTION        300

// Path turning directions: number of degrees to turn (positive, 
// anticlockwise) to face in the right direction for the next step.
#define PATH_TURN_LEFT   60
#define PATH_TURN_RIGHT 300  /* 300 degrees = -60 degrees (mod 360) */
#define PATH_TURN_BACK  180

// Paths to the initial campuses and arcs for each uni.
// These paths make the last hop through the initial arc to the campus.
#define INITIAL_PATH_A1 "RB"
#define INITIAL_PATH_A2 "RLLRRLRLRLR"
#define INITIAL_PATH_B1 "RLRRLRR"
#define INITIAL_PATH_B2 "RLRLLRLRLR"
#define INITIAL_PATH_C1 "LRLRL"
#define INITIAL_PATH_C2 "RLRLRLRRLR"

// Paths to the vertices attached to the retraining centres
#define PATH_RETRAINING_BPS_1      "RLRLRLRRL"
#define PATH_RETRAINING_BPS_2      "RLRLRLRRLL"
#define PATH_RETRAINING_BQN_1      "RLRLLRLRL"
#define PATH_RETRAINING_BQN_2      "RLRLLRLRLL"
#define PATH_RETRAINING_MJ_1       "RLLRRLRLRLL"
#define PATH_RETRAINING_MJ_2       "RLLRRLRLRLLL"
#define PATH_RETRAINING_MTV_1      "R"
#define PATH_RETRAINING_MTV_2      "RR"
#define PATH_RETRAINING_MMONEY_1   "LR"
#define PATH_RETRAINING_MMONEY_2   "LRL"

// KPI points that should be awarded
#define KPI_PER_ARC                     2
#define KPI_PER_CAMPUS                 10
#define KPI_PER_GO8                    20
#define KPI_PER_PATENT                 10
#define KPI_FOR_MOST_ARCS           10
#define KPI_FOR_MOST_PUBLICATIONS   10

// Types of retraining centre
//#define STUDENT_THD        0
//...
//#define STUDENT_MMONEY     5
#define RETRAINING_NONE      6

// Exchange rates between student types:
// For when the uni has access to a flexible retraining centre:
#define EXCHANGE_RATE_FLEXIBLE   3
// For when the uni has access to a discipline specific retraining centre
#define EXCHANGE_RATE_SPECIFIC   2

// The dice value for which all MTV and MMONEY students transfer to THDs.
#define THE_APOCALYPSE  7

//Maximum number of G08s allowed on the game
#define MAX_GO8S 8

// When this is true, the details of any interface function calls are
// printed to stdout
#define PRINT_INTERFACE_CALLS TRUE

// university struct: holds data about a uni
typedef struct _university {
    int arcs;            // The number of arc grants the uni has
    int normalCampuses;  // The number of normal campuses the uni has
    int GO8s;            // The number of GO8 campuses the uni has
    int publications;    // The number of publications the uni has
    int patents;         // The number of IP patents the uni has
    // The number of students of each discipline the uni has:
    int students[NUM_STUDENT_TYPES];
    // binary values indicating which retraining centres the uni has
    int retrainingCentres[NUM_STUDENT_TYPES];
} university;

// 2D vector: used to represent coordinates on the grid struct as well as
// vectors used to find locations relative to other objects
typedef struct _vector {
    int x;
    int y;
} vector;

// Region struct: holds information about a particular region
typedef struct _region {
    int diceValue;    // The dice value which will produce students
    int discipline;   // The type of students that will be produced
    vector location;  // The coordinates of the region in the grid struct
} region;

// Vertex struct: holds infomation about a particular vertex
typedef struct _vertex {
    int campus;           // The type of campus on the vertex
    int retrainingCentre; // The type of retraining centre on the vertex
    vector location;      // Coordinates of the vertex in the grid struct
} vertex;

// Edge struct: contains data particular edge
typedef struct _edge {
    int arc;          // Which uni has an arc grant on the edge
    vector location;  // The coordinates of the arc in the grid struct
} edge;

// A cell in the board struct. Contains a pointer to the relevant object.
typedef struct _cell {
    int objectType;     // The type of object at this location
    vertex* vertex;     // Pointer to the vertex at this location
    region* region;     // Pointer to the region at this location
    edge* edge;         // Pointer to the arc at this location
} cell;

// Board struct: a two dimensional grid of Cells representing the game
// board. The cells should be populated to represent the board.
// The bottom left corner is {0,0}.
typedef cell boardGrid[BOARD_COLUMNS][BOARD_ROWS];

// Game struct: stores all the persistent data about a game, including
// the state of the board and of all players
typedef struct _game {
    boardGrid board;      // A grid representing the layout of the board
    region regions[NUM_REGIONS];    // Data about each region
    vertex vertices[NUM_VERTICES];  // Data about each vertex
    edge edges[NUM_EDGES];          // Data about each arc
    university unis[NUM_UNIS + 1];   // Data about each uni
    // ^^ we need an extra index for the NO_ONE (UNI_A starts at 1)
    int activeUni;       // The uni who's turn it is
    int turnNumber;         // Rounds elapsed (-1 initially)
    int totalGO8s;          // Total number of GO8 campuses on the board
    int mostArcs;           // The uni with the most arc grants
    int mostPublications;   // The uni with the most publications
} game;



/// Local prototypes:

// Functions to go between the different sets of constants
static int campusOwner(int campusCode);
static int campusType(int campusCode);
static int campusForUni(int uni);
static int GO8ForUni(int uni);
static int arcForUni(int uni);
static int arcOwner(int arcCode);

// given two vectors, the sum of both is returned
static vector sum(vector vector1, vector vector2);

// given a vector and a scalar, the vector is scaled by the scalar
// Integer math only!
static vector scaleVector(vector input, int scalar);

// Given a vector, check whether it corresponds with a location on the
// board struct. (Ie it is not outside the bounds of the struct)
static int isOnBoard(vector location);

// Given a direction, these functions return the vector between objects
// of each type
static vector vectorBetweenEdges(int direction);
static vector vectorBetweenEdgeAndRegion(int direction);
static vector vectorBetweenRegionAndVertex(int direction);
static vector vectorBetweenEdgeAndVertex(int direction);
static vector vectorBetweenVertices(int direction);

// Print an error when we get an invalid path.
static void rejectPath(path badPath, char* reason);

// Find the coordinates of the object at the end of the path
static vector locationFromPath(boardGrid board,
                               path pathToVertex,
                               int objectType);

// Return a pointer to the required object at the path's end
static vertex* vertexFromPath(path pathToVertex, boardGrid board);
static edge* edgeFromPath(path pathToEdge, boardGrid board);

// Given the location of an object, this function returns a pointer to
// the required neighbouring object in the specified direction
static edge* relativeEdge(boardGrid board, vector base, int direction);
static vertex* relativeVertex(boardGrid board,
                              vector base,
                              int direction);

// Initialise the players and objects on the board. initBoard and
// initRegions msut be called first, in that order.
static void initBoard(boardGrid board);
static void initRegions(boardGrid board,
                        region regions[NUM_REGIONS],
                        int discipline[NUM_REGIONS],
                        int dice[]);
static void initVertices(boardGrid board,
                         region regions[],
                         vertex vertices[]);
static void initEdges(boardGrid board, region regions[], edge edges[]);
static void initUnis(university players[]);
                        
// Update which uni has prestige points for arcs/publications
static void updateArcPrestigePoints(Game g);
static void updatePublicationPrestigePoints(Game g);


// Return the strings representing commonly used constants for debugging
static char* uniString(int uni);
static char* campusString(int campus);
static char* arcString(int campus);
static char* disciplineString(int discipline);
static char* actionString(int actionCode);

// Print a table of students owned by each uni in each discipline
static void printStudentCounts(university players[]);

// Print the dice value and discipline for each region
static void printBoard(Game g);


/// Interface function definitions:


// make a new game, given the disciplines produced by each
// region, and the value on the dice discs in each region.
// note: each array must be NUM_REGIONS long
Game newGame (int discipline[], int dice[]) {
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: newGame Called\n");
    }
    Game g = malloc(sizeof(game));
    
    // Initialise regions, arcs, vertices, and players
    initBoard(g->board);
    initRegions(g->board, g->regions, discipline, dice);
    initEdges(g->board, g->regions, g->edges);
    initVertices(g->board, g->regions, g->vertices);
    initUnis(g->unis);

    
    // Put in the initial campuses:

    // university A:
    vertex* destVertex = vertexFromPath(INITIAL_PATH_A1, g->board);
    edge* destEdge = edgeFromPath(INITIAL_PATH_A1, g->board);
    destVertex->campus = CAMPUS_A;
    destEdge->arc = ARC_A;
    destVertex = vertexFromPath(INITIAL_PATH_A2, g->board);
    destEdge = edgeFromPath(INITIAL_PATH_A2, g->board);
    destVertex->campus = CAMPUS_A;
    destEdge->arc = ARC_A;

    // university B:
    destVertex = vertexFromPath(INITIAL_PATH_B1, g->board);
    destEdge = edgeFromPath(INITIAL_PATH_B1, g->board);
    destVertex->campus = CAMPUS_B;
    destEdge->arc = ARC_B;
    destVertex = vertexFromPath(INITIAL_PATH_B2, g->board);
    destEdge = edgeFromPath(INITIAL_PATH_B2, g->board);
    destVertex->campus = CAMPUS_B;
    destEdge->arc = ARC_B;

    // university C:
    destVertex = vertexFromPath(INITIAL_PATH_C1, g->board);
    destEdge = edgeFromPath(INITIAL_PATH_C1, g->board);
    destVertex->campus = CAMPUS_C;
    destEdge->arc = ARC_C;
    destVertex = vertexFromPath(INITIAL_PATH_C2, g->board);
    destEdge = edgeFromPath(INITIAL_PATH_C2, g->board);
    destVertex->campus = CAMPUS_C;
    destEdge->arc = ARC_C;


    // Set up the retraining centres: (each pair is the pair of vertices
    // connected to one retraining centre)

    destVertex = vertexFromPath(PATH_RETRAINING_BPS_1, g->board);
    destVertex->retrainingCentre = STUDENT_BPS;
    destVertex = vertexFromPath(PATH_RETRAINING_BPS_2, g->board);
    destVertex->retrainingCentre = STUDENT_BPS;

    destVertex = vertexFromPath(PATH_RETRAINING_BQN_1, g->board);
    destVertex->retrainingCentre = STUDENT_BQN;
    destVertex = vertexFromPath(PATH_RETRAINING_BQN_2, g->board);
    destVertex->retrainingCentre = STUDENT_BQN;

    destVertex = vertexFromPath(PATH_RETRAINING_MJ_1, g->board);
    destVertex->retrainingCentre = STUDENT_MJ;
    destVertex = vertexFromPath(PATH_RETRAINING_MJ_2, g->board);
    destVertex->retrainingCentre = STUDENT_MJ;

    destVertex = vertexFromPath(PATH_RETRAINING_MTV_1, g->board);
    destVertex->retrainingCentre = STUDENT_MTV;
    destVertex = vertexFromPath(PATH_RETRAINING_MTV_2, g->board);
    destVertex->retrainingCentre = STUDENT_MTV;
    
    destVertex = vertexFromPath(PATH_RETRAINING_MMONEY_1, g->board);
    destVertex->retrainingCentre = STUDENT_MMONEY;
    destVertex = vertexFromPath(PATH_RETRAINING_MMONEY_2, g->board);
    destVertex->retrainingCentre = STUDENT_MMONEY;
    
    // Initialise global game variables
    g->activeUni = NO_ONE;
    g->turnNumber = TERRA_NULLIUS;
    g->totalGO8s = 0;
    g->mostArcs = NO_ONE;
    g->mostPublications = NO_ONE;

    if (PRINT_INTERFACE_CALLS == TRUE) {
        printBoard(g);
    }
    
    return g;
}


// Given a game, this function destroys it and frees the memory it used
void disposeGame(Game g) {
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: disposing Game.\n");
    }
    free (g);
}


// make the specified action for the current uni and update the
// game state accordingly.
void makeAction (Game g, action a) {
    int activeUni = g->activeUni;
    
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: makeAction Called. Action: %s. ",
               actionString(a.actionCode));
        if (a.actionCode == BUILD_CAMPUS || a.actionCode == BUILD_GO8 ||
            a.actionCode == OBTAIN_ARC) {
            printf("Destination: \"%s\". ", a.destination);
        }
        if (a.actionCode == RETRAIN_STUDENTS) {
            printf("From %s ", disciplineString(a.disciplineFrom));
            printf("to %s. ", disciplineString(a.disciplineTo));
        }
        printf("It is %s's turn.\n", uniString(activeUni));
    }
    
    if (a.actionCode == PASS) {
        // Do nothing.
    } else if (a.actionCode == BUILD_CAMPUS) {
        // Update the uni's campus count:
        g->unis[activeUni].normalCampuses++;
        
        // Update the board to contain the campus:
        vertex* destVertex = vertexFromPath(a.destination, g->board);
        if (destVertex == NULL) {
            printf("Error: Game.c: null destVertex\n");
            return;
        }
        destVertex->campus = campusForUni(activeUni);

        // Deduct the students used to build the campus
        g->unis[activeUni].students[STUDENT_BPS]--;
        g->unis[activeUni].students[STUDENT_BQN]--;
        g->unis[activeUni].students[STUDENT_MJ]--;
        g->unis[activeUni].students[STUDENT_MTV]--;

        // If the new campus is attached to a retraining centre, note
        // this in the student struct for easy reference.
        if (destVertex->retrainingCentre != RETRAINING_NONE) {
            int type = destVertex->retrainingCentre;
            g->unis[activeUni].retrainingCentres[type] = TRUE;
        }
        
    } else if (a.actionCode == BUILD_GO8) {
        // Update the uni's GO8 count
        g->unis[activeUni].GO8s++;

        // Remove the normal campus to make room for the GO8 campus
        g->unis[activeUni].normalCampuses--;
        
        // Update the global GO8 count:
        g->totalGO8s++;
        
        // update the board to contain the GO8 campus
        vertex* destVertex = vertexFromPath(a.destination, g->board);
        if (destVertex == NULL) {
            printf("error: Game.c: null destVertex!\n");
            return;
        }
        destVertex->campus = GO8ForUni(activeUni);

        // Deduct the students used to build the GO8 campus
        g->unis[activeUni].students[STUDENT_MJ] -= 2;
        g->unis[activeUni].students[STUDENT_MMONEY] -= 3;
        
    } else if (a.actionCode == OBTAIN_ARC) {
        // Update the uni's arc count
        g->unis[activeUni].arcs++;
        
        //update the board to contain the arc grant
        edge* destEdge = edgeFromPath(a.destination, g->board);
        if (destEdge == NULL) {
            printf("Error: Game.c: null edge\n");
            return;
        }
        destEdge->arc = arcForUni(activeUni);

        // Deduct the students used to build the edge arc
        g->unis[activeUni].students[STUDENT_BQN]--;
        g->unis[activeUni].students[STUDENT_BPS]--;
        
        // Update the prestige points for haivng the most arc grants
        updateArcPrestigePoints(g);
        
    } else if (a.actionCode == OBTAIN_PUBLICATION) {
        // Deduct the students used to start the spinoff
        g->unis[activeUni].students[STUDENT_MJ]--;
        g->unis[activeUni].students[STUDENT_MTV]--;
        g->unis[activeUni].students[STUDENT_MMONEY]--;
        
        // Update the publication count for the uni:
        g->unis[activeUni].publications++;
        
        // update publications prestige points
        updatePublicationPrestigePoints(g);
        
    } else if (a.actionCode == OBTAIN_IP_PATENT) {
        // Deduct the students used to start the spinoff
        g->unis[activeUni].students[STUDENT_MJ]--;
        g->unis[activeUni].students[STUDENT_MTV]--;
        g->unis[activeUni].students[STUDENT_MMONEY]--;
        
        // Update the patent count for the uni:
        g->unis[activeUni].patents++;

    } else if (a.actionCode == RETRAIN_STUDENTS) {
        // Get student exchange rate between the requested disciplines
        int exchangeRate = getExchangeRate(g,
                                           activeUni,
                                           a.disciplineFrom,
                                           a.disciplineTo);

        // Update the student counts
        g->unis[activeUni].students[a.disciplineFrom] -= exchangeRate;
        g->unis[activeUni].students[a.disciplineTo]++;
    } else {
        printf("Error: Game.c: makeAction: Invalid action: ");
        printf("%d\n", a.actionCode);
    }
    
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: makeAction: student counts after action:\n");
        printStudentCounts(g->unis);
    }
}


// advance the game to the next turn, assuming that the dice has just
// been rolled and produced diceScore
void throwDice (Game g, int diceScore) {
    // Increment the turn number
    g->turnNumber++;

    // Use the modulus of the turn number to deduce whose turn it is
    if (g->turnNumber % 3 == 0) {
        g->activeUni = UNI_A;
    } else if (g->turnNumber % 3 == 1) {
        g->activeUni = UNI_B;
    } else if (g->turnNumber % 3 == 2) {
        g->activeUni = UNI_C;
    }
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: throwDice Called with dice: %d. ", diceScore);
        printf("turnNumber: %d. ", g->turnNumber);
        printf("It is %s's turn.\n", uniString(g->activeUni));
    }
    // For each region with the same dice score that was just rolled, all
    // players must be awarded 1 student of the type that region produces
    // for each campus they have in the region (2 for a GO8 campus)
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        if (g->regions[regionID].diceValue == diceScore) {
            
            // Find which student type that region produces
            int discipline = g->regions[regionID].discipline;
            
            // Find the coordinates of the region
            vector regionLocation = g->regions[regionID].location;
            
            // Check all the neighbouring vertices for campuses
            for (int dir = DIR_RIGHT; dir < NUM_DIRECTIONS; dir++) {
                vertex* adjacentVertex;
                adjacentVertex = relativeVertex(g->board,
                                                regionLocation,
                                                dir);
                if (adjacentVertex != NULL) {
                    // Find the uni which owns the campus
                    int owner = campusOwner(adjacentVertex->campus);

                    // Award students depending on the campus type
                    if (campusType(adjacentVertex->campus) ==
                                                    NORMAL_CAMPUS) {
                        g->unis[owner].students[discipline]++;
                    } else if (campusType(adjacentVertex->campus) ==
                                                        GO8_CAMPUS) {
                        g->unis[owner].students[discipline] += 2;
                    }
                }
            }
        }
    }

    // If a seven is rolled, all MTV and MMONEY students change to THDs
    // immediately after the students are produced
    if (diceScore == THE_APOCALYPSE) {
        for (int uni = UNI_A; uni <= UNI_C; uni++) {
            g->unis[uni].students[STUDENT_THD] +=
                        g->unis[uni].students[STUDENT_MTV];
            g->unis[uni].students[STUDENT_THD] +=
                        g->unis[uni].students[STUDENT_MMONEY];
            g->unis[uni].students[STUDENT_MTV] = 0;
            g->unis[uni].students[STUDENT_MMONEY] = 0;
        }
    }
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: throwDice: student counts after changes:\n");
        printStudentCounts(g->unis);
    }
}


// returns TRUE if it is legal for the current
// uni to make the specified move, FALSE otherwise.
// It is not legal to make any action during Terra Nullius ie
// before the game has started.
// It is not legal for a uni to make the moves OBTAIN_PUBLICATION
// or OBTAIN_IP_PATENT (they can make the move START_SPINOFF)
int isLegalAction (Game g, action a) {
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: isLegalAction Called. Action: %s. ",
               actionString(a.actionCode));
        if (a.actionCode == BUILD_CAMPUS || a.actionCode == BUILD_GO8 ||
            a.actionCode == OBTAIN_ARC) {
            printf("Destination: \"%s\".", a.destination);
        }
        if (a.actionCode == RETRAIN_STUDENTS) {
            printf("From %s ", disciplineString(a.disciplineFrom));
            printf("to %s", disciplineString(a.disciplineTo));
        }
        printf("\n");
    }
    
    int uni = g->activeUni;
    int isLegal; // Whether or not the action is legal
    
    if (a.actionCode == PASS) {
        // It is always legal to pass
        isLegal = TRUE;
        
    } else if (a.actionCode == BUILD_CAMPUS) {
        vertex* baseVertex = vertexFromPath(a.destination, g->board);
        if (baseVertex == NULL) {
            printf("Error: Game.c: null vertex\n");
            return FALSE;
        }
        
        // Check if there are any campuses on the adjacent vertices
        int hasRoom = FALSE;
        // There is room unless there are campuses in adjacent vertices
        hasRoom = TRUE;
        for (int dir = DIR_RIGHT; dir < NUM_DIRECTIONS; dir++) {
            vertex* adjacentVertex;
            adjacentVertex = relativeVertex(g->board,
                                            baseVertex->location,
                                            dir);
            if (adjacentVertex != NULL) {
                if (adjacentVertex->campus != VACANT_VERTEX) {
                    // There is an adjacent vertex with a campus on it
                    hasRoom = FALSE;
                }
            }
        }
        
        // Check if the uni has a arc on any of the adjacent edges
        int hasArc = FALSE;
        for (int dir = DIR_RIGHT; dir < NUM_DIRECTIONS; dir++) {
            edge* destEdge = relativeEdge(g->board,
                                             baseVertex->location,
                                             dir);
            if (destEdge != NULL) {
                if (arcOwner(destEdge->arc) == uni) {
                    hasArc = TRUE;
                }
            }
        }
        
        // Get how many of the relevant students the uni has
        int BPSs = g->unis[uni].students[STUDENT_BPS];
        int BQNs = g->unis[uni].students[STUDENT_BQN];
        int MJs = g->unis[uni].students[STUDENT_MJ];
        int MTVs = g->unis[uni].students[STUDENT_MTV];
        
        // Check if the uni has enough of each to build a campus
        int hasStudents = FALSE;
        if (BPSs >= 1 && BQNs >= 1 && MJs >= 1 && MTVs >= 1) {
            hasStudents = TRUE;
        }
        
        // Building a campus is legal if all three conditions are met
        if (hasRoom == TRUE && hasArc == TRUE && hasStudents == TRUE) {
            isLegal = TRUE;
        } else {
            isLegal = FALSE;
        }
        
    } else if (a.actionCode == BUILD_GO8) {
        // Check if the uni has the prerequisite campus at the vertex
        vertex* destVertex = vertexFromPath(a.destination, g->board);
        if (destVertex == NULL) {
            printf("Error: Game.c: null destVertex\n");
            return FALSE;
        }
        int hasCampus = FALSE;
        if (campusOwner(destVertex->campus) == uni &&
            campusType(destVertex->campus) == NORMAL_CAMPUS) {
            hasCampus = TRUE;
        }

        // Get how many of the relevant students the uni has
        int MJs = g->unis[uni].students[STUDENT_MJ];
        int MMs = g->unis[uni].students[STUDENT_MMONEY];
        
        // Check if the uni has enough students to build a GO8
        int hasEnoughStudents = FALSE;
        if (MJs >= 2 && MMs >= 3) {
            hasEnoughStudents = TRUE;
        }
        
        // Check if there are already 8 GO8s
        int tooManyGO8s = TRUE;
        if (g->totalGO8s < MAX_GO8S) {
            tooManyGO8s = FALSE;
        }
        
        // If all three requirements are met, the action is legal
        if (hasEnoughStudents == TRUE && hasCampus == TRUE &&
            tooManyGO8s == FALSE) {
            isLegal = TRUE;
        } else {
            isLegal = FALSE;
        }
            
    } else if (a.actionCode == OBTAIN_ARC) {
        // Check if the edge is vacant
        edge* destEdge = edgeFromPath(a.destination, g->board);
        if (destEdge == NULL) {
            printf("Error: Game.c: null edge\n");
            return FALSE;
        }
        int edgeEmpty = FALSE;
        if (destEdge->arc == VACANT_ARC) {
            edgeEmpty = TRUE;
        }
        
        // Check if the uni already has an arc adjacent to this edge
        int HasPath = FALSE;
        for (int dir = DIR_RIGHT; dir < NUM_DIRECTIONS; dir++) {
            edge* adjacentEdge;
            adjacentEdge = relativeEdge(g->board,
                                        destEdge->location,
                                        dir);
            if (adjacentEdge != NULL) {
                if (arcOwner(adjacentEdge->arc) == uni) {
                    // The uni has a arc on an adjacent arc
                    HasPath = TRUE;
                }
            }
        }
        
        // Check whether the uni has enough students for an arc grant
        int BQNs = g->unis[uni].students[STUDENT_BQN];
        int BPSs = g->unis[uni].students[STUDENT_BPS];
        int hasStudents = FALSE;
        if (BQNs >= 1 && BPSs >= 1) {
            hasStudents = TRUE;
        }
        
        // The move is legal if all three requirements are met
        if (edgeEmpty == TRUE && hasStudents == TRUE &&
            HasPath == TRUE) {
            isLegal = TRUE;
        } else {
            isLegal = FALSE;
        }
            
    } else if (a.actionCode == START_SPINOFF) {
        // Get the number of relevant students
        int MJs = g->unis[uni].students[STUDENT_MJ];
        int MTVs = g->unis[uni].students[STUDENT_MTV];
        int MMs = g->unis[uni].students[STUDENT_MMONEY];
        
        // Check if the uni has enough students
        if (MJs >= 1 && MTVs >= 1 && MMs >= 1) {
            isLegal = TRUE;
        } else {
            isLegal = FALSE;
        }
        
    } else if (a.actionCode == RETRAIN_STUDENTS) {
        
        // Get the exchange rate between the requested students types
        int exchangeRate = getExchangeRate(g,
                                           uni,
                                           a.disciplineFrom,
                                           a.disciplineTo);
        
        // Check how many of the source students the uni has
        int studentsFrom = g->unis[uni].students[a.disciplineFrom];
        
        // If the uni has enough students, the action is legal
        if (studentsFrom >= exchangeRate) {
            isLegal = TRUE;
        } else {
            isLegal = FALSE;
        }

        // Exception: THDs Cannot be retrained
        if (a.disciplineFrom == STUDENT_THD) {
            isLegal = FALSE;
        }
        
    } else {
        printf("Error: Game.c: isLegalAction received invalid");
        printf(" actionCode %d\n", a.actionCode);
        isLegal = FALSE; // The action code was invalid
    }

    if (uni < UNI_A) {
        // All actions are illegal if it is nobody's turn
        isLegal = FALSE;
    }
    return isLegal;
}


//Get the turn number. The first turn is 0. -1 before the game starts.
int getTurnNumber (Game g) {
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getTurnNumber called. returning ");
        printf("%d\n", g->turnNumber);
    }
    return g->turnNumber;
}


// Get the type of students produced by the specified region.
int getDiscipline (Game g, int regionID) {
    int result = g->regions[regionID].discipline;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getDiscipline called for region %d. ", regionID);
        printf("Returning %s\n", disciplineString(result));
    }
    return result;
}


// Get the dice value which produces students in a region (2..12).
int getDiceValue (Game g, int regionID) {
    int result = g->regions[regionID].diceValue;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getDiceValue called for region %d. ", regionID);
        printf("Returning %d\n", result);
    }
    return result;
}


// Get which university has the most ARCs (NO_ONE until the first arc is
// obtained).
int getMostARCs (Game g) {
    int result = g->mostArcs;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getMostArcs called. ");
        printf("Returning %s\n", uniString(result));
    }
    return result;
}


// Get which university has the most publications (NO_ONE until the first
// IP is patented).
int getMostPublications (Game g) {
    int result = g->mostPublications;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getMostPublications called. ");
        printf("Returning %s\n", uniString(result));
    }
    return result;
}


// Get the uni who's turn it is. Returns NO_ONE during Terra Nullius.
int getWhoseTurn (Game g) {
    int result = g->activeUni;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getWhoseTurn called. ");
        printf("Returning %s\n", uniString(result));
    }
    return result;
}


// Get the campus code for the given vertex
int getCampus(Game g, path pathToVertex) {
    vertex* destVertex = vertexFromPath(pathToVertex, g->board);
    if (destVertex == NULL) {
        printf("Error: Game.c: getCampus: null destVertex\n");
        return VACANT_VERTEX;
    }
    int result = destVertex->campus;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getCampus called for path ");
        printf("\"%s\". ", pathToVertex);
        printf("Returning %s\n", campusString(result));
    }
    return result;
}


// Get the contents of the given edge
int getARC(Game g, path pathToEdge) {
    edge* destEdge = edgeFromPath(pathToEdge, g->board);
    if (destEdge == NULL) {
        printf("Error: Game.c: null edge\n");
        return VACANT_ARC;
    }
    int result = destEdge->arc;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getARC called for path \"%s\". ", pathToEdge);
        printf("Returning %s\n", arcString(result));
    }
    return result;
}


// Get the number of KPI points the specified uni has
int getKPIpoints (Game g, int uni) {
    int kpi = 0;
    kpi += KPI_PER_ARC * g->unis[uni].arcs;
    kpi += KPI_PER_CAMPUS * g->unis[uni].normalCampuses;
    kpi += KPI_PER_GO8 * g->unis[uni].GO8s;
    kpi += KPI_PER_PATENT * g->unis[uni].patents;

    if (g->mostArcs == uni) {
        kpi += KPI_FOR_MOST_ARCS;
    }

    if (g->mostPublications == uni) {
        kpi += KPI_FOR_MOST_PUBLICATIONS;
    }
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getKPIpoints called for %s.", uniString(uni));
        printf(" Returning %d\n", kpi);
    }
    return kpi;
}


// Get the number of arc grants the specified uni has
int getARCs (Game g, int uni) {
    int result = g->unis[uni].arcs;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getArcs called for %s. ", uniString(uni));
        printf("Returning %d\n", result);
    }
    return result;
}


// Get the number of GO8 campuses the specified uni has
int getGO8s (Game g, int uni) {
    int result = g->unis[uni].GO8s;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getGO8s called for %s. ", uniString(uni));
        printf("Returning %d\n", result);
    }
    return result;
}


// Get the number of normal Campuses the specified uni has
int getCampuses (Game g, int uni) {
    int result = g->unis[uni].normalCampuses;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getCampuses called for %s", uniString(uni));
        printf(". Returning %d\n", result);
    }
    return result;
}


// Get the number of IP Patents the specified uni has
int getIPs (Game g, int uni) {
    int result = g->unis[uni].patents;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getIPs called for %s. ", uniString(uni));
        printf("Returning %d\n", result);
    }
    return result;
}


// Get the number of publications the specified uni has
int getPublications (Game g, int uni) {
    int result = g->unis[uni].publications;
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getPublications called for ");
        printf("%s", uniString(uni));
        printf(". Returning %d\n", result);
    }
    return result;
}


// Get the number of students a given uni has in a given discipline
int getStudents (Game g, int uni, int discipline) {
    int result = g->unis[uni].students[discipline];
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getStudents called for %s", uniString(uni));
        printf(" in discipline %s. ", disciplineString(discipline));
        printf("Returning %d\n", result);
    }
    return result;
}


// Determine how many students of discipline type disciplineFrom the
// specified uni would need to retrain in order to get one student of
// discipline type disciplineTo. The result will depend on what
// retraining centers, if any, they have a campus at.
int getExchangeRate (Game g,
                     int uni,
                     int disciplineFrom,
                     int disciplineTo) {
    int result;
    university* activeUni = &g->unis[g->activeUni];
    int hasCentre = activeUni->retrainingCentres[disciplineFrom];
    if (hasCentre == TRUE) {
        result = EXCHANGE_RATE_SPECIFIC;
    } else {
        result = EXCHANGE_RATE_FLEXIBLE;
    }
    if (PRINT_INTERFACE_CALLS == TRUE) {
        printf("Game.c: getExchangeRate called for ");
        printf("%s ", uniString(uni));
        printf("from %s ", disciplineString(disciplineFrom));
        printf("to %s. ", disciplineString(disciplineTo));
        printf("Returning %d\n", result);
    }
    return result;
}

/// Local function definitions:

// Given a game struct, this function updates the values for which uni
// has the most arc grants, based on the values for each uni
static void updateArcPrestigePoints(Game g) {
    int arcsA = g->unis[UNI_A].arcs;
    int arcsB = g->unis[UNI_B].arcs;
    int arcsC = g->unis[UNI_C].arcs;
    
    if (arcsA > arcsB && arcsA > arcsC) {
        g->mostArcs = UNI_A;
    } else if (arcsB > arcsA && arcsB > arcsC) {
        g->mostArcs = UNI_B;
    } else if (arcsC > arcsB && arcsC > arcsA) {
        g->mostArcs = UNI_C;
    }
    // If none of them is more than the other two, nothing should be done
    // This ensures that prestige points are maintained until another
    // uni builds more arcs than the one who has the points
}

// Given a game struct, this function updates the values for which uni
// has the most publications, based on the values for each uni
static void updatePublicationPrestigePoints(Game g) {
    int pubsA = g->unis[UNI_A].publications;
    int pubsB = g->unis[UNI_B].publications;
    int pubsC = g->unis[UNI_C].publications;
    
    if (pubsA > pubsB && pubsA > pubsC) {
        g->mostPublications = UNI_A;
    } else if (pubsB > pubsA && pubsB > pubsC) {
        g->mostPublications = UNI_B;
    } else if (pubsC > pubsB && pubsC > pubsA) {
        g->mostPublications = UNI_C;
    }
    // If none of them is more than the other two, nothing should be done
    // This ensures that prestige points are maintained until another
    // uni gets more publications than the one who has the points
}


// Given a board, regions, and arrays of the disciplines and dice values
// of each region, the regions will be initialised
static void initRegions(boardGrid board,
                        region regions[NUM_REGIONS],
                        int discipline[NUM_REGIONS],
                        int dice[]) {
    // First note the locations of all the regions (in order by regionID)
    vector locations[NUM_REGIONS] = {
        { 3,14}, { 3,10}, { 3, 6},
        { 7,16}, { 7,12}, { 7, 8}, { 7, 4},
        {11,18}, {11,14}, {11, 10}, {11, 6}, {11, 2},
        {15,16}, {15,12}, {15, 8}, {15, 4},
        {19,14}, {19,10}, {19, 6}
    };
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        // Set the location, discipline, and dice value of each region
        regions[regionID].location = locations[regionID];
        regions[regionID].discipline = discipline[regionID];
        regions[regionID].diceValue = dice[regionID];
        
        // Update the board struct with a reference to each region
        cell* boardCell;
        boardCell = &board[locations[regionID].x][locations[regionID].y];
        boardCell->objectType = OBJECT_REGION;
        boardCell->region = &regions[regionID];
    }
}


// Given a board and regions (already initialised), this function will
// Initialise the vertices.
static void initVertices(boardGrid board,
                         region regions[],
                         vertex vertices[]) {
    int vertexNo = 0;
    // For each region, initialise a vertex on each corner of the hexagon
    // with the region as its centre (only once for each vertex)
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        for (int dir = 0; dir < DEGREES_IN_CIRCLE; dir += 60) {
            // Find the location of each vertex from the region
            vector location = sum(regions[regionID].location,
                                  vectorBetweenRegionAndVertex(dir));
            cell* boardCell = &board[location.x][location.y];

            if (boardCell->objectType == OBJECT_NONE) {
                // Record the vertex in the board struct
                boardCell->objectType = OBJECT_VERTEX;
                boardCell->vertex = &vertices[vertexNo];

                // Initialise the values of the vertex struct
                vertices[vertexNo].location = location;
                vertices[vertexNo].campus = VACANT_VERTEX;
                vertices[vertexNo].retrainingCentre = RETRAINING_NONE;
                vertexNo++;
            }
        }
    }
}


// Given a board and regions (already initialised), this function will
// Initialise the arcs.
static void initEdges(boardGrid board, region regions[], edge edges[]) {
    int arcNo = 0;
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        for (int dir = 30; dir < DEGREES_IN_CIRCLE; dir += 60) {
            // Find the location of each arc near the region
            vector location = sum(regions[regionID].location,
                                    vectorBetweenEdgeAndRegion(dir));
            assert(isOnBoard(location));
            // For the two invalid directions 90 and 270,
            // location will simply be the location of the given region,
            // so the check below will fail and nothing will be done
            cell* boardCell = &board[location.x][location.y];

            if (boardCell->objectType == OBJECT_NONE) {

                // Record a reference to the arc in the board struct
                boardCell->objectType = OBJECT_EDGE;
                boardCell->edge = &edges[arcNo];

                // Set the initial values of the arc
                edges[arcNo].location = location;
                edges[arcNo].arc = VACANT_ARC;
                arcNo++;
            }
        }
    }
}


// Given an array of players, set their values to initial values
static void initUnis(university players[]) {
    for (int uni = UNI_A; uni <= UNI_C; uni++) {
        // Each uni starts with one campus and one arc grant
        players[uni].arcs = 2;
        players[uni].normalCampuses = 2;
        players[uni].GO8s = 0;
        players[uni].publications = 0;
        players[uni].patents = 0;
        for (int type = 0; type < NUM_STUDENT_TYPES; type++) {
            players[uni].students[type] = 0;
            players[uni].retrainingCentres[type] = FALSE;
        }
    }
}


// Given a board, set all its cells to contain nothing
static void initBoard(boardGrid board) {
    // Initialise grid struct to contain empty cells
    for (int column = 0; column < BOARD_COLUMNS; column++) {
        for (int row = 0; row < BOARD_ROWS; row++) {
            board[column][row].objectType = OBJECT_NONE;
            board[column][row].region = NULL;
            board[column][row].vertex = NULL;
            board[column][row].edge = NULL;
        }
    }
}


// Print an error when we get an invalid path.
static void rejectPath(path badPath, char* reason) {
    printf("Error: Game.c: received an invalid path: \"%s\". ", badPath);
    printf("reason: %s\n", reason);
}


// Given a path and whether an edge or vertex should be returned, this
// function returns the coordinates of the object at the end of the path
static vector locationFromPath(boardGrid board,
                               path pathToVertex,
                               int objectType) {
    // Start from the defined root of a path
    vector location = PATH_START_LOCATION;
    int direction = PATH_INITIAL_DIRECTION;
    vector defaultResult = {0,0}; // returned for invalid paths
    // Read and process each character until we reach the end
    for (int pathIndex = 0; pathToVertex[pathIndex] != 0; pathIndex++) {
        // First add the last turn to the direction so that the direction
        // stores the direction to the next vertex
        if (pathToVertex[pathIndex] == 'L') {
            direction += PATH_TURN_LEFT;
        } else if (pathToVertex[pathIndex] == 'R') {
            direction += PATH_TURN_RIGHT;
        } else if (pathToVertex[pathIndex] == 'B') {
            direction += PATH_TURN_BACK;
        } else {
            rejectPath(pathToVertex, "Contains an invalid character");
            printf("ascii: %d\n", (int) pathToVertex[pathIndex]);
            return defaultResult;
        }
        direction %= DEGREES_IN_CIRCLE;
        
        // Move to the next vertex
        location = sum(location, vectorBetweenVertices(direction));
        
        // At each step, make sure we are still on the board
        if (isOnBoard(location)) {
            // and that we are still on the island:
            if (board[location.x][location.y].vertex == NULL) {
                rejectPath(pathToVertex, "goes off island");
                return defaultResult;
            }
        } else {
            rejectPath(pathToVertex, "goes off board");
            return defaultResult;
        }
        
        // Make sure the path is under the maximum length
        if (pathIndex >= PATH_LIMIT) {
            rejectPath(pathToVertex, "longer than 150 characters");
            return defaultResult;
        }
    }
    
    // Now return the location
    if (objectType == OBJECT_VERTEX) {
        return location;
    } else if (objectType == OBJECT_EDGE) {
        // Special case: "" is not valid for arcs.
        if (pathToVertex[0] == 0) {
            rejectPath(pathToVertex, "Empty path not valid for arcs");
            return defaultResult;
        }
        // Go backwards to the arc we just passed
        direction += 180;
        direction %= DEGREES_IN_CIRCLE;
        // Since the above ensures that the path is valid, there is no
        // need to check if the arc exists.
        return sum(location, vectorBetweenEdgeAndVertex(direction));
    } else {
        printf("Error: Game.c: locationFromPath: Invalid objectType\n");
        return defaultResult;
    }
}


// Given a path, this function returns a pointer to the vertex at its end
static vertex* vertexFromPath(path pathToVertex, boardGrid board) {
    vector location = locationFromPath(board,
                                       pathToVertex,
                                       OBJECT_VERTEX);
    return board[location.x][location.y].vertex;
}


// Given a path, this function returns a pointer to the edge at its end
static edge* edgeFromPath(path pathToEdge, boardGrid board) {
    vector location = locationFromPath(board, pathToEdge, OBJECT_EDGE);
    return board[location.x][location.y].edge;
}


// Given the location of an object, this function returns a pointer to
// the neighbouring arc in the specified direction
static edge* relativeEdge(boardGrid board, vector base, int dir) {
    vector location; // This will hold the coordinates of the arc
    if (!isOnBoard(base)) {
        printf("Error: Game.c: relativeEdge given invalid location");
        return NULL;
    }
    // Depending on the base object type, add the appropriate vector.
    if (board[base.x][base.y].objectType == OBJECT_EDGE) {
        location = sum(base, vectorBetweenEdges(dir));
    } else if (board[base.x][base.y].objectType == OBJECT_REGION) {
        location = sum(base, vectorBetweenEdgeAndRegion(dir));
    } else if (board[base.x][base.y].objectType == OBJECT_VERTEX) {
        location = sum(base, vectorBetweenEdgeAndVertex(dir));
    } else {
        printf("Game.c: invalid base object passed to relativEdge\n");
        return NULL;
    }
    // Return the pointer to the required arc. This will be null if there
    // is no arc there
    if (isOnBoard(location)) {
        return board[location.x][location.y].edge;
    } else {
        return NULL;
    }
}


// Given the location of an object, this function returns a pointer to
// the neighbouring vertex in the specified direction
static vertex* relativeVertex(boardGrid board, vector base, int dir) {
    vector location; // This will hold the coordinates of the arc
    if (!isOnBoard(base)) {
        printf("Error: Game.c: relativeVertex given invalid location");
        return NULL;
    }
    // Depending on the base object type, add the appropriate vector.
    if (board[base.x][base.y].objectType == OBJECT_EDGE) {
        location = sum(base, vectorBetweenEdgeAndVertex(dir));
    } else if (board[base.x][base.y].objectType == OBJECT_REGION) {
        location = sum(base, vectorBetweenRegionAndVertex(dir));
    } else if (board[base.x][base.y].objectType == OBJECT_VERTEX) {
        location = sum(base, vectorBetweenVertices(dir));
    } else {
        printf("Game.c: invalid base object passed to relativeRegion\n");
        return NULL;
    }
    // Return the pointer to the required vertex. This will be null if
    // there is no vertex there
    if (isOnBoard(location)) {
        return board[location.x][location.y].vertex;
    } else {
        return NULL;
    }
}


// Given two vectors, the sum of both is returned
static vector sum(vector vector1, vector vector2) {
    vector result;
    result.x = vector1.x + vector2.x;
    result.y = vector1.y + vector2.y;
    return result;
}


// Given a vector and a scalar, the vector is scaled by the scalar
// Integer math only!
static vector scaleVector(vector input, int scalar) {
    input.x *= scalar;;
    input.y *= scalar;
    return input;
}


// Given a vector, check whether it corresponds with a location on the
// board struct. (Ie it is not outside the bounds of the struct)
static int isOnBoard(vector location) {
    if ( (0 <= location.x && location.x < BOARD_COLUMNS) &&
        (0 <= location.y && location.y < BOARD_ROWS) ) {
        return TRUE;
        } else {
            return FALSE;
        }
}


// Given a direction, this function returns the vector from an edge to the
// adjacent edge in the given direction. If the given direction is
// invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
static vector vectorBetweenEdges(int direction) {
    vector result;
    if (direction == 30 || direction == DIR_RIGHT_ABOVE) {
        result.x = 2;
        result.y = 1;
    } else if (direction == 90 || direction == DIR_ABOVE) {
        result.x = 0;
        result.y = 2;
    } else if (direction == 150 || direction == DIR_LEFT_ABOVE) {
        result.x = -2;
        result.y = 1;
    } else if (direction == 210 || direction == DIR_LEFT_BELOW) {
        result.x = -2;
        result.y = -1;
    } else if (direction == 270 || direction == DIR_BELOW) {
        result.x = 0;
        result.y = -2;
    } else if (direction == 330 || direction == DIR_RIGHT_BELOW) {
        result.x = 2;
        result.y = -1;
    } else {
        // The direction given was not valid
        result.x = 0;
        result.y = 0;
    }
    return result;
}


// Given a direction, this function returns the vector from an arc to the
// adjacent region in the given direction, and vice versa. If the given
// direction is invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
static vector vectorBetweenEdgeAndRegion(int direction) {
    return vectorBetweenEdges(direction);
}


// Given a direction, this function returns the vector from a region to
// the adjacent vertex in the given direction, or vice versa. If the
// given direction is invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
static vector vectorBetweenRegionAndVertex(int direction) {
    vector result;
    if (direction == 0 || direction == DIR_RIGHT) {
        result.x = 3;
        result.y = 0;
    } else if (direction == 60 || direction == DIR_RIGHT_ABOVE) {
        result.x = 1;
        result.y = 2;
    } else if (direction == 120 || direction == DIR_LEFT_ABOVE) {
        result.x = -1;
        result.y = 2;
    } else if (direction == 180 || direction == DIR_LEFT) {
        result.x = -3;
        result.y = 0;
    } else if (direction == 240 || direction == DIR_LEFT_BELOW) {
        result.x = -1;
        result.y = -2;
    } else if (direction == 300 || direction == DIR_RIGHT_BELOW) {
        result.x = 1;
        result.y = -2;
    } else {
        // The direction given was not valid
        result.x = 0;
        result.y = 0;
    }
    return result;
}


// Given a direction, this function returns the vector from an edge to
// the adjacent vertex in the given direction, or vice versa. If the
// given direction is invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
static vector vectorBetweenEdgeAndVertex(int direction) {
    vector result;
    if (direction == 0 || direction == DIR_RIGHT) {
        result.x = 1;
        result.y = 0;
    } else if (direction == 60 || direction == DIR_RIGHT_ABOVE) {
        result.x = 1;
        result.y = 1;
    } else if (direction == 120 || direction == DIR_LEFT_ABOVE) {
        result.x = -1;
        result.y = 1;
    } else if (direction == 180 || direction == DIR_ABOVE) {
        result.x = -1;
        result.y = 0;
    } else if (direction == 240 || direction == DIR_LEFT_BELOW) {
        result.x = -1;
        result.y = -1;
    } else if (direction == 300 || direction == DIR_RIGHT_BELOW) {
        result.x = 1;
        result.y = -1;
    } else {
        // The direction given was not valid
        result.x = 0;
        result.y = 0;
    }
    return result;
}


// Given a direction, this function returns the vector from a vertex to
// the adjacent vertex in the given direction. If the given direction is
// invalid, {0,0} is returned.
// The direction can be one of the defined directions, or it can be in
// degrees (assuming regular hexagons)
static vector vectorBetweenVertices(int direction) {
    return scaleVector(vectorBetweenEdgeAndVertex(direction), 2);
}


// Given a campus code, return the uni that owns it
static int campusOwner(int campusCode) {
    int result;
    if (campusCode == CAMPUS_A || campusCode == GO8_A) {
        result = UNI_A;
    } else if (campusCode == CAMPUS_B || campusCode == GO8_B) {
        result = UNI_B;
    } else if (campusCode == CAMPUS_C || campusCode == GO8_C) {
        result = UNI_C;
    } else if (campusCode == VACANT_VERTEX) {
        result = NO_ONE;
    } else {
        printf("\nError: Game.c: campusOwner: invalid campusCode: %d\n",
               campusCode);
        result = -1;
    }
    return result;
}


// Given a campus code, return what type of campus it is
static int campusType(int campusCode) {
    int result;
    if (campusCode == CAMPUS_A ||
        campusCode == CAMPUS_B ||
        campusCode == CAMPUS_C) {
        result = NORMAL_CAMPUS;
    } else if (campusCode == GO8_A ||
        campusCode == GO8_B ||
        campusCode == GO8_C ) {
        result = GO8_CAMPUS;
    } else if (campusCode == VACANT_VERTEX) {
        result = VACANT_VERTEX;
    } else {
        printf("\nError: Game.c: campusType: invalid campusCode:");
        printf(" %d\n", campusCode);
        result = -1;
    }
    return result;
}


// Given a uni, return the code for a campus owned by that uni
static int campusForUni(int uni) {
    int result;
    if (uni == UNI_A) {
        result = CAMPUS_A;
    } else if (uni == UNI_B) {
        result = CAMPUS_B;
    } else if (uni == UNI_C) {
        result = CAMPUS_C;
    } else if (uni == NO_ONE) {
        result = VACANT_VERTEX;
    } else {
        result = -1;
        printf("\nError: Game.c: campusForUni: invalid uni: %d\n", uni);
    }
    return result;
}


// Given a uni, return the code for a GO8 owned by that uni
static int GO8ForUni(int uni) {
    int result;
    if (uni == UNI_A) {
        result = GO8_A;
    } else if (uni == UNI_B) {
        result = GO8_B;
    } else if (uni == UNI_C) {
        result = GO8_C;
    } else if (uni == NO_ONE) {
        result = VACANT_VERTEX;
    } else {
        result = -1;
        printf("\nError: Game.c: GO8ForUni: invalid uni: %d\n", uni);
    }
    return result;
}


// Given a uni, return the code for an an arc owned by that uni
static int arcForUni(int uni) {
    int result;
    if (uni == UNI_A) {
        result = ARC_A;
    } else if (uni == UNI_B) {
        result = ARC_B;
    } else if (uni == UNI_C) {
        result = ARC_C;
    } else if (uni == NO_ONE) {
        result = VACANT_ARC;
    } else {
        result = -1;
        printf("\nError: Game.c: arcForUni: invalid uni: %d\n", uni);
    }
    return result;
}


// Given an arc code, return the uni that the arc
static int arcOwner(int arcCode) {
    int result;
    if (arcCode == ARC_A) {
        result = UNI_A;
    } else if (arcCode == ARC_B) {
        result = UNI_B;
    } else if (arcCode == ARC_C) {
        result = UNI_C;
    } else if (arcCode == VACANT_ARC) {
        result = NO_ONE;
    } else {
        result = -1;
        printf("\nError: Game.c: arcOwner: invalid arcCode: ");
        printf("%d\n", arcCode);
    }
    return result;
}


// Given a uni, this function returns a string giving its name
static char* uniString(int uni) {
    char* result;
    if (uni == NO_ONE) {
        result = "NO_ONE";
    } else if (uni == UNI_A) {
        result = "UNI_A";
    } else if (uni == UNI_B) {
        result = "UNI_B";
    } else if (uni == UNI_C) {
        result = "UNI_C";
    } else {
        printf("\nError: Game.c: uniString: invalid uni\n");
        result = "INVALID_UNI";
    }
    return result;
}


// Given a campus code, this function returns a string giving its name
static char* campusString(int campus) {
    char* result;
    if (campus == VACANT_VERTEX) {
        result = "VACANT_VERTEX";
    } else if (campus == CAMPUS_A) {
        result = "CAMPUS_A";
    } else if (campus == CAMPUS_B) {
        result = "CAMPUS_B";
    } else if (campus == CAMPUS_C) {
        result = "CAMPUS_C";
    } else if (campus == GO8_A) {
        result = "GO8_A";
    } else if (campus == GO8_B) {
        result = "GO8_B";
    } else if (campus == GO8_C) {
        result = "GO8_C";
    } else {
        printf("\nError: Game.c: campusString: invalid campusCode\n");
        result = "INVALID_CAMPUS_CODE";
    }
    return result;
}


// Given an arc code, this function returns a string giving its name
static char* arcString(int campus) {
    char* result;
    if (campus == VACANT_ARC) {
        result = "VACANT_ARC";
    } else if (campus == ARC_A) {
        result = "ARC_A";
    } else if (campus == ARC_B) {
        result = "ARC_B";
    } else if (campus == ARC_C) {
        result = "ARC_C";
    } else {
        printf("\nError: Game.c: arcString: invalid arc code\n");
        result = "INVALID_ARC_CODE";
    }
    return result;
}


// Given a uni, this function returns a string giving its name
static char* disciplineString(int discipline) {
    char* result;
    if (discipline == STUDENT_THD) {
        result = "THD";
    } else if (discipline == STUDENT_BPS) {
        result = "BPS";
    } else if (discipline == STUDENT_BQN) {
        result = "BQN";
    } else if (discipline == STUDENT_MJ) {
        result = "MJ";
    } else if (discipline == STUDENT_MTV) {
        result = "MTV";
    } else if (discipline == STUDENT_MMONEY) {
        result = "MON";
    } else {
        printf("\nError: Game.c: disciplineString: invalid discipline:");
        printf(" %d\n", discipline);
        result = "INVALID_DISCIPLINE";
    }
    return result;
}


// Given an action code, this function returns a string giving its name
static char* actionString(int actionCode) {
    char* result;
    if (actionCode == PASS) {
        result = "PASS";
    } else if (actionCode == BUILD_CAMPUS) {
        result = "BUILD_CAMPUS";
    } else if (actionCode == BUILD_GO8) {
        result = "BUILD_GO8";
    } else if (actionCode == OBTAIN_ARC) {
        result = "OBTAIN_ARC";
    } else if (actionCode == START_SPINOFF) {
        result = "START_SPINOFF";
    } else if (actionCode == OBTAIN_PUBLICATION) {
        result = "OBTAIN_PUBLICATION";
    } else if (actionCode == OBTAIN_IP_PATENT) {
        result = "OBTAIN_IP_PATENT";
    } else if (actionCode == RETRAIN_STUDENTS) {
        result = "RETRAIN_STUDENTS";
    } else {
        result = "INVALID_ACTION";
        printf("\nError: Game.c: actionString: invalid action: ");
        printf("%d\n", actionCode);
    }
    return result;
}


// Print a table of students owned by each uni in each discipline
static void printStudentCounts(university players[]) {
    printf("\n  UNI    THD    BPS    BQN     MJ    MTV   MMON\n");
    for (int uni = UNI_A; uni <= UNI_C; uni++) {
        printf("%s", uniString(uni));
        for (int type = 0; type < NUM_STUDENT_TYPES; type++) {
            printf("     %2d", players[uni].students[type]);
        }
        printf("\n");
    }
    printf("\n");
}


// Print the dice value and discipline for each region
static void printBoard(Game g) {
    printf("Printing board:\n");
    printf("RegionID diceValue discipline\n");
    for (int regionID = 0; regionID < NUM_REGIONS; regionID++) {
        int dice = g->regions[regionID].diceValue;
        int discipline = g->regions[regionID].discipline;
        printf("      %2d        %2d", regionID, dice);
        printf("   %s\n", disciplineString(discipline));
    }
    printf("\n");
}
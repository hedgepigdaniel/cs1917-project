/*
 *  printGame.c
 *  print out an ASCII version of the game map
 *  version 1.0  
 *
 *  Created by Tim Lambert on 23/04/12.
 *  Copyright 2012 Licensed under Creative Commons SA-BY-NC 3.0. 
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "Game.h"

#define BOARDROWS 11
#define BOARDCOLS 21
//max no of arcs on map
#define MAXNOARCS 100


typedef struct _arc {
  int row;
  int col;
  int sign;
  path pth;
} arc;

typedef struct _place {
  int row;
  int col;
} place;


typedef char board[BOARDROWS][BOARDCOLS];

static void fillBoard(Game g, board b);
static int visit(Game g, arc e, board b);
static int start(int row);
static int end(int row);
static int valid(arc e);
static arc right(arc e);
static arc left(arc e);
char campusToChar(int code);

//print an ASCII representation of the game
void printGame(Game g) {
  char * dicekey1 = "          111";
  char * dicekey2 = " 123456789012";
  char * disciplinekey = "BRCYGM";
  place tilepos[] = {{1,6},{3,4},{5,2},
			{1,10},{3,8},{5,6},{7,4},
			{1,14},{3,12},{5,10},{7,8},{9,6},
			       {3,16},{5,14},{7,12},{9,10},
			{5,18},{7,16},{9,14}};
  int i,j;
  int dice;
  int regionID;
  board b;
  place p;
  
  fillBoard(g,b);
  for (regionID=0; regionID < NUM_REGIONS; regionID++) {
    p = tilepos[regionID];
    dice = getDiceValue(g, regionID);
    b[p.row][p.col-1] = dicekey1[dice];
    b[p.row][p.col] = dicekey2[dice];
    b[p.row][p.col+1] = disciplinekey[getDiscipline(g, regionID)];
  }
  putchar('\n');
  for (i=0; i < BOARDROWS; i++){
    for (j=0; j < BOARDCOLS; j++){
      putchar(b[i][j]);
    }
    putchar('\n');
  }

 
}

//breadth first search to find a path to each arc on board
static void fillBoard(Game g, board b){
  arc queue[MAXNOARCS];
  int i,j;
  int head = 0;//of queue
  int tail = 1;//of queue
  arc e = {-1,14,1,""}; //start of all paths
  arc next;

  for (i=0; i < BOARDROWS; i++){
    for (j=0; j < BOARDCOLS; j++){
      b[i][j] =' ';
    }
  }
  //special case of campus at end of empty path
  //which is at position(0,14) on our board
  b[0][14] = campusToChar(getCampus(g, ""));

  queue[0] = e;
  while (head < tail){
    e = queue[head];
    //printf("%d %d %d %s\n",head , e.row, e.col, e.pth);
    head++;
    next = right(e);
    if (visit(g, next, b)) {
      queue[tail] = next;
      tail++;
    }
    next = left(e);
    if (visit(g, next, b)) {
      queue[tail] = next;
      tail++;
    }
  }
}

//visit an arc, filling in the board with a character
// such as a, b, c, -, + for the arc and the adjacent vertex
// returns TRUE if we update the board
// rturn FALSE if we don't have to do anything because we've
// already visited this spot
static int visit(Game g, arc e, board b){

  int answer;
  int arcContents;
  int campus;
  char arcChar; //character representation
  answer = FALSE;
  if(valid(e) && b[e.row][e.col] == ' '){
    answer = TRUE;
    arcContents = getARC(g, e.pth);
    campus = getCampus(g, e.pth);
    if (e.row % 2 == 0) {
      arcChar = '-';
      b[e.row][e.col+e.sign] = campusToChar(campus);
    } else {
      arcChar = '|';
      b[e.row+e.sign][e.col] = campusToChar(campus);
    }
    if (arcContents != VACANT_VERTEX) {
      arcChar = campusToChar(arcContents);
    }
    b[e.row][e.col] = arcChar;
    
  }
  return answer;
}


//start position of board on a row
static int start(int row) {
  return 2*(abs(row - BOARDROWS/2)/2);
}

//end position of board on a row
static int end(int row) {
  return BOARDCOLS - start(row) - 1;
}

// is this arc on the map or has it gone into the ocean?
static int valid(arc e){
  return 0 <= e.row && e.row < BOARDROWS &&
    start(e.row) <= e.col && e.col <= end(e.row);
}

//where you get if you go right from an arc
static arc right(arc e){
  arc answer;
  strcpy(answer.pth,e.pth);
  strcat(answer.pth,"R");
  if(e.row%2 != 0){
    answer.row = e.row + e.sign;
    answer.col = e.col - e.sign;
    answer.sign = - e.sign;
  } else if((e.row+e.col)%4 == 1) {
    answer.row = e.row;
    answer.col = e.col + 2*e.sign;
    answer.sign = e.sign;
  } else {
    answer.row = e.row + e.sign;
    answer.col = e.col + e.sign;
    answer.sign = e.sign;

  }
  return answer;
}

//where you get if you go left from an arc
static arc left(arc e){
  arc answer;
  strcpy(answer.pth,e.pth);
  strcat(answer.pth,"L");
  if(e.row%2 != 0){
    answer.row = e.row + e.sign;
    answer.col = e.col + e.sign;
    answer.sign = e.sign;
  } else if((e.row+e.col)%4 == 1) {
    answer.row = e.row - e.sign;
    answer.col = e.col + e.sign;
    answer.sign = - e.sign;
  } else {
    answer.row = e.row;
    answer.col = e.col + 2*e.sign;
    answer.sign = e.sign;

  }
  return answer;
}


//turn a campus code into a character for printing
// '+' is an empty vertex, a,b,c are campuses A,B,C are GO8 campuses
char campusToChar(int code){
  char * key = "+abcABC";
  return key[code];
}

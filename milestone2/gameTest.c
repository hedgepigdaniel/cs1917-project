/*
 * gameTest.c
 * Daniel Playfair Cal
 * for fixing all teh segfaults in Game.c
 * 13/05/12
 */

#include "Game.h"

#include <stdio.h>
#include <stdlib.h>


int main (int arc, char* argv[]) {
    int discipline [NUM_REGIONS] = {1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4};
    int dice [NUM_REGIONS] = {1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4};
    Game game = newGame(discipline[], dice[]);

    return EXIT_SUCCESS;
}
// print an ASCII representation of the game to stdout
/*
The map with the starting position of each player
and region ids:

    b-+-+-+-+aa-+    
    b 0 | 3 | 7 |    
  +-+-+-+-+-+-+-+-+  
  | 1 | 4 | 8 |12 |  
+-+-+-+-+-+-+-+-+-+cc
| 2 | 5 | 9 | 13| 16|
cc+-+-+-+-+-+-+-+-+-+
  | 6 | 10| 14| 17|  
  +-+-+-+-+-+-+-+-+  
    | 11| 15| 18b    
    +-aa+-+-+-+-b    
*/

void printGame(Game g);

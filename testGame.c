/*
 *  testGame.c
 *  1917-w123
 *
 *  Created by Richard Buckland on 28/04/11.
 *  Copyright 2011 Licensed under Creative Commons SA-BY-NC 3.0. 
 *
 *
 *  Used for Comp1917 assignment 2012 by Radin, Daniel, Natalie, Denaysh.
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Game.h"

#define CYAN STUDENT_BQN
#define PURP STUDENT_MMONEY
#define YELL STUDENT_MJ
#define RED STUDENT_BPS
#define GREE STUDENT_MTV 
#define BLUE STUDENT_THD

#define DEFAULT_DISCIPLINES {CYAN,PURP,YELL,PURP,YELL,RED ,GREE,GREE, RED,GREE,CYAN,YELL,CYAN,BLUE,YELL,PURP,GREE,CYAN,RED }
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,9,9,2,8,10,5}
void testNewGameBasic (void);
void testGetTurnNumber (void);

int main (int argc, const char * argv[]) {
   testNewGameBasic ();  
   testGetTurnNumber ();  
   printf ("All tests passed!  you are Awesome\n");
   return EXIT_SUCCESS;
}

void testNewGameBasic (void) {
   printf ("testing new Game (basic tests) ...");
   // check makes a game without crashing 
   int disciplines[NUM_REGIONS];
   int dice [NUM_REGIONS];	
   
   int regionID = 0;
   while (regionID < NUM_REGIONS) {
      disciplines[regionID] = STUDENT_THD;
      dice[regionID] = 12;
      regionID++;
   }

   Game g = newGame (disciplines, dice);
   assert (getTurnNumber(g) == -1);
   

   // check initial KPI points for each player
   assert(getKPIpoints (game, UNI_A) == 24);
   assert(getKPIpoints (game, UNI_B) == 24);
   assert(getKPIpoints (game, UNI_C) == 24);
   printf("Everyone starts with 24 KPI\n");

   // check each player has 2 campusus and 2 arc grants
   assert(getARCs (game, UNI_A) == 2);
   assert(getCampuses (game, UNI_A) == 2);
   assert(getARCs (game, UNI_B) == 2);
   assert(getCampuses (game, UNI_B) == 2);
   assert(getARCs (game, UNI_C) == 2);
   assert(getCampuses (game, UNI_C) == 2);
   printf("Each player has 2 initial ARCs and Campusus\n");

   // check that player A can pass turn
   assert(getWhoseTurn(game) == UNI_A);
   action a;
   a.actionCode = PASS;
   assert(isLegalAction(game, a));
   makeAction(game, a));

   // check that player B can pass turn
   assert(getWhoseTurn(game) == UNI_B);
   action a;
   a.actionCode = PASS;
   assert(isLegalAction(game, a));
   makeAction(game, a));

   // check that player C can pass turn
   assert(getWhoseTurn(game) == UNI_C);
   action a;
   a.actionCode = PASS;
   assert(isLegalAction(game, a));
   makeAction(game, a));

   // test to ensure game does not exceed score limit
   assert(getKPIpoints(game, UNI_A) <= 150);
   assert(getKPIpoints(game, UNI_B) <= 150);
   assert(getKPIpoints(game, UNI_C) <= 150);
   	
   disposeGame (g);
   printf ("Passed\n");

   //arcs don't overlap 
   //1 camp/g08 per arc
   // test arc action
   // test camp/g08 action
   // implment student patient or publication
}
                      
void testGetTurnNumber (void) {
   printf ("testing get turn number...");
   int disciplines[] = DEFAULT_DISCIPLINES;
   int dice[] = DEFAULT_DICE;
   Game g = newGame (disciplines, dice);
   
   assert (getTurnNumber(g) == -1);

   // Test that player turns are being cycled correctly
   assert(getWhoseTurn(game) == UNI_A);
   throwDice(game, 7);
   assert(getWhoseTurn(game) == UNI_B);	
   throwDice(game, 7);
   assert(getWhoseTurn(game) == UNI_C);
   throwDice(game, 7);	
   assert(getWhoseTurn(game) == UNI_A);

   // Test that players are getting appropriate students for rolls
   throwDice(game, 8);
   assert(getStudents(game, UNI_C, STUDENT_MJ) == 1);
   assert(getStudents(game, UNI_B, STUDENT_MTV) == 1);
   throwDice(game, 8);
   assert(getStudents(game, UNI_C, STUDENT_MJ) == 2);
   assert(getStudents(game, UNI_B, STUDENT_MTV) == 2);
   throwDice(game, 3);
   assert(getStudents(game, UNI_A, STUDENT_BPS) == 1);	

   // Test to see if roll 7 transfers students to THDs
   throwDice(game, 7);
   assert(getStudents(game, UNI_B, STUDENT_MTV) == 1);
   assert(getStudents(game, UNI_B, STUDENT_THD) == 1);
   
   // Check too see if retraining works x3 MJ for 1 MMONEY
   throwDice(game, 8); 
   throwDice(game, 8);
   throwDice(game, 8);
   throwDice(game, 8);
   assert(getStudents(game, UNI_C, STUDENT_MJ) == 5);
   assert(getWhoseTurn(game) == UNI_C);
   action a;
   a.actionCode = RETRAIN_STUDENTS;
   assert(getStudents(game, UNI_C, STUDENT_MJ) == 1);
   assert(getStudents(game, UNI_C, STUDENT_MMONEY) == 1);

   // Start a spin-off action test
   // first trade x3 MJ for 1 MTV
   throwDice(game, 8); 
   throwDice(game, 8);
   throwDice(game, 8);
   action a;
   a.actionCode = RETRAIN_STUDENTS;  
   a.actionCode = START_SPINOFF;


   disposeGame (g);
   printf ("Passed\n");
}


